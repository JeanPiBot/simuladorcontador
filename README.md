# Simulador de un contador digital

Diseñe un algoritmo para simular  un contador digital de unidades (U), decenas (D), y centenas (C) desde el conteo 000 hasta 999, de tal manera que cada vez que se alcance el dígito 9 en las unidades se avanc al siguiente digito en las decenas; y cada vez que se alcance el número 9 en las decenas se avance un dígito más en las centenas hasta completar el tope del contador.


# Run and Building

If you just want to run, you can use a command which is:

```
go run sumaMulti.go
```

Then build it with the go tool:

```
go build sumaMulti.go
```

The command above will build an executable named main in the current directory alongside your source code. Execute it to see extraction of data:

```
./sumaMulti.go
```

__NOTE:__ Just is executable for UNIX system. For execute into Windows system click on the main.exe



# Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.


# Authors
* **Jean Pierre Giovanni Arenas Ortiz**

# License
[MIT](https://choosealicense.com/licenses/mit/)
