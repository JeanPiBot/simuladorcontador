package main

import "fmt"


func main() {
	for c := 0; c <= 9; c++ {
		for d := 0; d <= 9; d++ {
			for u := 0; u <= 9; u++ {
				fmt.Printf("Centenas %d Decenas %d Unidades %d\n", c, d, u)
			}
		}
	}
}